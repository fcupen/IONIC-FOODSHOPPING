(function(){

var app = angular.module('mynotes', ['ionic','mynotes.notestore','firebase'])

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    } 
  });
}) 

app.config(function($stateProvider, $urlRouterProvider){
	
	$stateProvider.state('pedidos',{
		url:'/pedidos',
		templateUrl:'templates/pedidos.html',
		controller: 'PedidosCtrl'
	});
	$stateProvider.state('histop',{
		url:'/histop',
		templateUrl:'templates/histop.html',
		controller: 'HistoPCtrl'
	});
	$stateProvider.state('pincompleto',{
		url:'/pincompleto',
		templateUrl:'templates/pincompleto.html',
		controller: 'PIncompletoCtrl'
	});
	$stateProvider.state('histo',{
		url:'/histo',
		templateUrl:'templates/histo.html',
		controller: 'HistoCtrl'
	});
	$stateProvider.state('entregarTodos',{
		url:'/entregartodos',
		templateUrl:'templates/entregar_todos.html',
		controller: 'EntregarTCtrl'
	});
	$stateProvider.state('entregas',{
		url:'/entregas',
		templateUrl:'templates/entregas.html',
		controller: 'EntregasCtrl'
	});
	$stateProvider.state('espera',{
		url:'/espera',
		templateUrl:'templates/espera.html',
		controller: 'EsperaCtrl'
	});
	$stateProvider.state('add_c', {
        url: '/add_c',
        templateUrl: 'templates/add_combo.html',
        controller: 'AddCombo'
    });
	$stateProvider.state('lugar', {
        url: '/lugar',
        templateUrl: 'templates/lugar.html',
        controller: 'LugarCtrl'
    });
	$stateProvider.state('registro', {
        url: '/registro',
        templateUrl: 'templates/registro.html',
        controller: 'RegistroCtrl'
    });
	$stateProvider.state('pagar', {
        url: '/pagar',
        templateUrl: 'templates/pagar.html',
        controller: 'PagarCtrl'
    });
	$stateProvider.state('add_f', {
        url: '/add_f',
        templateUrl: 'templates/add_food.html',
        controller: 'AddFood'
    });
	$stateProvider.state('carrito', {
        url: '/carrito',
        templateUrl: 'templates/carrito.html',
        controller: 'CarritoCtrl'
    });
	$stateProvider.state('info', {
        url: '/info',
        templateUrl: 'templates/info.html',
        controller: 'InfoCtrl'
    });
	$stateProvider.state('add_i', {
        url: '/add_i/:noteId',
        templateUrl: 'templates/add_ingredientes.html',
        controller: 'AddIng'
    });
	$stateProvider.state('add_it', {
        url: '/add_it/:noteId',
        templateUrl: 'templates/add_ingredientes_c.html',
        controller: 'AddIngt'
    });
	$stateProvider.state('food',{
		url:'/food',
		templateUrl:'templates/todo.html'
	});
	$stateProvider.state('login',{
		url:'/',
		templateUrl:'templates/login.html'
	});
	
	$stateProvider.state('add',{
		url:'/add', 
		templateUrl:'tpl/edit.html',
		controller: 'AddCtrl'
	});
	$stateProvider.state('entregar',{
		url:'/entregar/:dirId',
		templateUrl:'templates/entregar.html',
		controller: 'EntregarCtrl'
	});
	$stateProvider.state('edit',{
		url:'/edit/:noteId',
		templateUrl:'tpl/edit.html',
		controller: 'EditCtrl'
	});
	$urlRouterProvider.otherwise('/');
});

//var notes =[];
//---



//---
	/*function getNote(noteId){
		for (var i = 0; i < notes.length; i++){
			if(notes[i].id === noteId){
				return notes[i];
			}
		}
		return undefined;
	}
	
	function updateNote(note){
		for (var i = 0; i < notes.length; i++){
			if(notes[i].id === note.id){
				notes[i] = note;
				return;
			}
		}
	}
	function createNote(note){
		notes.push(note);
	}*/
app.controller("LoginController", function( $scope, $state, $firebaseAuth, $ionicModal, $location) {
 var fb = new Firebase("https://tazon.firebaseio.com/comidas");
    $scope.login = function(username, password) {
        var fbAuth = $firebaseAuth(fb);
        fbAuth.$authWithPassword({
            email: username,
            password: password
        }).then(function(authData) {
            $location.path("/food");
        }).catch(function(error) {
            console.error("ERROR: " + error);
        });
    }
 
    $scope.register = function(username, password) {
        var fbAuth = $firebaseAuth(fb);
        fbAuth.$createUser({email: username, password: password}).then(function() {
            return fbAuth.$authWithPassword({
                email: username,
                password: password
            });
        }).then(function(authData) {
            $location.path("/food");
        }).catch(function(error) {
            console.error("ERROR " + error);
        });
    }
 
});
app.controller('ListCtrl', function($scope, NoteStore,$firebaseArray){
	fb = new Firebase("https://tazon.firebaseio.com/comidas");
	//var refh = new Firebase(fb);
	$scope.notes = $firebaseArray(fb);
	
});
//--
app.controller("AddFood", function( $scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray) {
	var ref = new Firebase("https://tazon.firebaseio.com/comidas/");
	var fireb =$firebaseArray(ref);

	$scope.food = {  
		id: new Date().getTime().toString(),
	};
	
	$scope.add_f = function(food){
		fireb.$add($scope.food);  
		$state.go('food'); 
	}
});

app.controller("AddCombo", function( $scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray) {
	var ref = new Firebase("https://tazon.firebaseio.com/combo/");
	var fireb =$firebaseArray(ref);

	$scope.combo = {  
		id: new Date().getTime().toString(),
	};
	
	$scope.add_c = function(combo){
		fireb.$add($scope.combo);  
		$state.go('food'); 
	}
});

app.controller("AddIng", function($scope, $location, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray) {
	var ref = new Firebase("https://tazon.firebaseio.com/comidas/"); 
	$scope.id = $state.params.noteId;
	ref.on("value", function(allMessagesSnapshot) {
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
		var id = messageSnapshot.child("id").val(); 
		if(messageSnapshot.child("id").val() == $scope.id){ 
		$scope.key = messageSnapshot.key();  
		console.log($scope.key);
		$scope.id=$scope.id;
		$scope.nombre=messageSnapshot.child("nombre").val();
		$scope.ingrediente=messageSnapshot.child("ingrediente").val();
		}		
	  }); 
	}); 
	
	
	var ref = new Firebase("https://tazon.firebaseio.com/comidas/"+$scope.key+"/descripcion/");
	var fireb =$firebaseArray(ref);
 
	$scope.ing = {  
		id: new Date().getTime().toString(),
		ingrediente: $scope.ingrediente,
		add:true //distancia del taxista
	};
	$scope.add_ing = function(ing){
		fireb.$add($scope.ing);  
		$state.go('food'); 
		 
	}
	
});
app.controller("AddIngt", function($scope, $location, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray) {
	var ref = new Firebase("https://tazon.firebaseio.com/combo/"); 
	$scope.id = $state.params.noteId;
	ref.on("value", function(allMessagesSnapshot) {
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
		var id = messageSnapshot.child("id").val(); 
		if(messageSnapshot.child("id").val() == $scope.id){ 
		$scope.key = messageSnapshot.key();  
		console.log($scope.key);
		$scope.id=$scope.id;
		$scope.nombre=messageSnapshot.child("nombre").val();
		$scope.ingrediente=messageSnapshot.child("ingrediente").val();
		}		
	  }); 
	}); 
	
	
	var ref = new Firebase("https://tazon.firebaseio.com/combo/"+$scope.key+"/descripcion/");
	var fireb =$firebaseArray(ref);
 
	$scope.ing = {  
		id: new Date().getTime().toString(),
		ingrediente: $scope.ingrediente,
		add:true //distancia del taxista
	};
	$scope.add_ing = function(ing){
		fireb.$add($scope.ing);  
		$state.go('food'); 
		 
	}
	
});


app.controller('TodoController', function( $rootScope, $timeout, $http,$compile, NoteStore,$scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray) {
	
	
	
function reloj() {
	
    var hoy=new Date(); var h=hoy.getHours(); var m=hoy.getMinutes(); var s=hoy.getSeconds();

    m = actualizarHora(m);    s = actualizarHora(s);
	//<div id ="displayReloj" style="float:left;" >   </div><div style="float:left; padding: 7px;" >Esperando un Pedido</div>
	//<div id ="displayReloj" style="float:left;" >   </div><div style="float:left; padding: 7px;" >Esperando un Pedido</div>
    //document.getElementById('displayReloj').innerHTML = h+":"+m+":"+s; numero % 2 == 0
	if(s % 2 == 0){$scope.seg = '';document.getElementById('displayReloj').innerHTML= '<i style="font-size:35px;  color:#2492FF" class="right ion-android-restaurant"></i>';}else{$scope.seg = '';document.getElementById('displayReloj').innerHTML= '<i style="font-size:35px; padding: 15px 0px 15px 0px; height:35px; color:#0066CC" class="right ion-android-restaurant"></i>';};

	var t = setTimeout(function(){reloj()},500);
	

$scope.seg = s;
}
 reloj();

function actualizarHora(i) {

    if (i<10) {i = "0" + i};  // Añadir el cero en números menores de 10

    return i;

}

	
	
	//---------------------------------
	//fin menu der 
	$scope.pre = function(id) {
    $scope.id = Math.round(document.getElementById(id).value / 1) * 1;
}
		
		//
		var urlfirebase = "https://tazon.firebaseio.com/";
		$scope.urlfirebase_comida = "https://tazon.firebaseio.com/comidas/";
		$scope.urlfirebase_sub = "https://tazon.firebaseio.com/carrito/";
		var refh = new Firebase(urlfirebase);
		refh.onAuth(function(authData) {
			
		if (authData) {
		$scope.ID = authData.uid;
        console.log("Authenticated with uid:", authData.uid);
		console.log("Authenticated with uid:", authData);
		} else {
        console.log("Client unauthenticated.");
		}
		});
		var refac = new Firebase("https://tazon.firebaseio.com/pedido/"+$scope.ID+"/");
		$scope.pedi =$firebaseArray(refac);
		console.log($scope.pedi);
		//--*/
		var fb2 = new Firebase("https://tazon.firebaseio.com/comidas");
		$scope.comidas = $firebaseArray(fb2);
		
		var user_info_fb = new Firebase("https://tazon.firebaseio.com/user"+$scope.ID+"/");
		$scope.user_info = $firebaseArray(user_info_fb);
		
		var fb3 = new Firebase("https://tazon.firebaseio.com/combo");
		$scope.combos = $firebaseArray(fb3);
		
		//--
		var urlfirebasesub =  new Firebase("https://tazon.firebaseio.com/carrito/"+$scope.ID);
		$scope.sub = $firebaseArray(urlfirebasesub);

		var aux;
		var cc;
		var dd;
		var aa;
		var ee=0;
		urlfirebasesub.on("value", function(allMessagesSnapshot) {
			ee=0;
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
		aa=messageSnapshot.child("cantidad").val();
		bb=messageSnapshot.child("precio").val();
		
		console.log(aa);
		console.log(bb);
		cc=aa*bb;
		ee=cc+ee
		$scope.aux=ee; 
		console.log(ee);
		if(messageSnapshot.child("id").val() == $scope.id){  
		}	
		
	  }); 
	});
		
		var ref = new Firebase("https://tazon.firebaseio.com/comidas/-KDVr8AcCRLHgDcNEswt/descripcion/");
		var fireb =$firebaseArray(ref);
		
var fbcheck =  new Firebase("https://tazon.firebaseio.com/prefactura/"+$scope.ID);
		$scope.check=false;
		fbcheck.on("value", function(allMessagesSnapshot) {
			var jsg=0;
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
			aa=messageSnapshot.child("state").val();
			if(aa.add == 'pre'){jsg=jsg+1;};
				if(jsg >= 1){$scope.check=true;}else{$scope.check=false;};
		  }); 
		});
		
var fbcheck_datos =  new Firebase("https://tazon.firebaseio.com/user/"+$scope.ID);
		$scope.check_datos=false;var jsga=0;
		fbcheck_datos.on("value", function(allMessagesSnapshot) {
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
			aa=messageSnapshot.child("nombre").val();
			if(aa.nombre != ''){jsga=1;};
		  }); 
		});
		fbcheck_datos.on("value", function(allMessagesSnapshot) {
if(jsga == 1){$scope.check_datos=false;}else{$scope.check_datos=true;};});

$scope.f_ing = function(id){
	$scope.id = id;
		$state.go('add_i/'+$scope.id); 
		
	};


var refa = new Firebase("https://tazon.firebaseio.com/carrito/"+$scope.ID);
var fireba =$firebaseArray(refa);
	
$scope.carrito = function(todo){
	//$scope.todo2 = {id: new Date().getTime().toString(),todo};
	var sub=todo.precio*todo.cantidad;
	fireba.$add({
		id: new Date().getTime().toString(),
		comida_id:todo.id,
		nombre:todo.nombre,
		img:todo.img,
		descripcion:todo.descripcion,
		cantidad:todo.cantidad,
		precio:todo.precio,
		subtotal:sub
	
	}); 
	
	console.log(todo.id);
	//document.getElementById(todo.id).value = '';
	
	};
/*
    $scope.create = function() {
    $ionicPopup.prompt({
        title: 'Enter a new TODO item',
        inputType: 'text'
    })
    .then(function(result) {
        if(result !== "") {
            if($scope.data.hasOwnProperty("comidas") !== true) {
                $scope.data.todos = [];
            }
            $scope.data.todos.push({title: result});
        } else {
            console.log("Action not completed");
        }
    });
}*/

});
//--
app.controller('AddCtrl', function($scope, $state, NoteStore){
	
	$scope.note = {
		id: new Date().getTime().toString(),
		title: '',
		description:''
		
	};
	
	$scope.save = function(){
		NoteStore.create($scope.note);
		$state.go('list');
	}
});

app.controller('CarritoCtrl', function($rootScope,$http,$compile, NoteStore,$scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray){
	var refac = new Firebase("https://tazon.firebaseio.com/pedido/"+$scope.ID+"/");
	$scope.pedi =$firebaseArray(refac);
	console.log($scope.pedi);
function reloj() {
	
    var hoy=new Date(); var h=hoy.getHours(); var m=hoy.getMinutes(); var s=hoy.getSeconds();

    m = actualizarHora(m);    s = actualizarHora(s);
	//<div id ="displayReloj" style="float:left;" >   </div><div style="float:left; padding: 7px;" >Esperando un Pedido</div>
	//<div id ="displayReloj" style="float:left;" >   </div><div style="float:left; padding: 7px;" >Esperando un Pedido</div>
    //document.getElementById('displayReloj').innerHTML = h+":"+m+":"+s; numero % 2 == 0
	if(s % 2 == 0){
		$scope.seg = '';document.getElementById('pedidocarrito').innerHTML= '<i style="font-size:35px;  color:#2492FF" class="right ion-android-restaurant"></i>';}else{
		$scope.seg = '';document.getElementById('pedidocarrito').innerHTML= '<i style="font-size:35px; padding: 15px 0px 15px 0px; height:35px; color:#0066CC" class="right ion-android-restaurant"></i>';};

	var t = setTimeout(function(){reloj()},500);
	

$scope.seg = s;
}
 reloj();

function actualizarHora(i) {

    if (i<10) {i = "0" + i};  // Añadir el cero en números menores de 10

    return i;

}
	
	//--
	var urlfirebase = "https://tazon.firebaseio.com/";
		var refh = new Firebase(urlfirebase);
		refh.onAuth(function(authData) {
		if (authData) {
		$scope.ID = authData.uid;
		} else {
        console.log("Client unauthenticated.");
		}
		});
	$rootScope.contP=0;
	var fbcheck =  new Firebase("https://tazon.firebaseio.com/prefactura/"+$scope.ID);
		$scope.check=false;
		fbcheck.on("value", function(allMessagesSnapshot) {
			var jsg=0;
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
			aa=messageSnapshot.child("state").val();
			if(aa.add == 'pre'){jsg=jsg+1;};
				if(jsg >= 1){$scope.check=true;}else{$scope.check=false;};
		  }); 
		});
	
	
	
	var urlfirebasesub =  new Firebase("https://tazon.firebaseio.com/carrito/"+$scope.ID);
		$scope.sub = $firebaseArray(urlfirebasesub);
		
		
		
		var aux;
		var cc;
		var dd;
		var aa;
		var ee=0;
		urlfirebasesub.on("value", function(allMessagesSnapshot) {
			ee=0;
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
		aa=messageSnapshot.child("cantidad").val();
		bb=messageSnapshot.child("precio").val();
		cc=aa*bb;
		ee=cc+ee
		$scope.aux=ee; 
	  }); 
	});
	
	var refa =  new Firebase("https://tazon.firebaseio.com/carrito/"+$scope.ID);
	var refc =  new Firebase("https://tazon.firebaseio.com/prefactura/"+$scope.ID+'/');
	var refb = new Firebase("https://tazon.firebaseio.com/comidas/");
	$scope.asd=refa;
	
	$scope.carritos=$firebaseArray(refc);
	refa.on("value", function(allMessagesSnapshot) {
	$scope.items =$firebaseArray(refa);});
	$scope.editIt = function(todo,todos,add){
		//console.log(todos);
		//console.log(todo.$id);
		if(add==true){add=false;}else{add=true;};
	
	var hopp2 =refa.child('/'+todo.$id+'/descripcion/'+todos+'/');
		hopp2.update({
			'add': add
	}); 
	}; 
	$scope.deleteIt = function(food,cantidad){
		var hopp3 =refa.child('/'+food.$id+'/');
		
			if(cantidad == '1'){$scope.items.$remove(food); }else{hopp3.update({'cantidad': cantidad-1});};
		//$scope.items.$remove(food); 
		if($scope.items.length == '1'){$scope.aux =0;}
		//$scope.aux=$scope.aux-(food.precio*food.cantidad);
		console.log(food);
		console.log($scope.items.length)
	}
	$scope.pagar = function(food){  
	console.log(food);
	$scope.items2=food;
	$scope.items2.state=({
			id: new Date().getTime().toString(),
			'add': "pre",
			"total":$scope.aux
	});  
	refa.remove();  
	var ref = new Firebase("https://tazon.firebaseio.com/prefactura/"+$scope.ID+"/");
	var fireb =$firebaseArray(ref);
		fireb.$add($scope.items2);  
		$state.go('lugar'); 
	}
});

app.controller('PagarCtrl', function($rootScope,$http,$compile, NoteStore,$scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray){
	var refac = new Firebase("https://tazon.firebaseio.com/pedido/"+$scope.ID+"/");
	$scope.pedi =$firebaseArray(refac);
	
function reloj() {
	
    var hoy=new Date(); var h=hoy.getHours(); var m=hoy.getMinutes(); var s=hoy.getSeconds();

    m = actualizarHora(m);    s = actualizarHora(s);
	//<div id ="displayReloj" style="float:left;" >   </div><div style="float:left; padding: 7px;" >Esperando un Pedido</div>
	//<div id ="displayReloj" style="float:left;" >   </div><div style="float:left; padding: 7px;" >Esperando un Pedido</div>
    //document.getElementById('displayReloj').innerHTML = h+":"+m+":"+s; numero % 2 == 0
	if(s % 2 == 0){
		$scope.seg = '';document.getElementById('pagarcarrito').innerHTML= '<i style="font-size:35px;  color:#2492FF" class="right ion-android-restaurant"></i>';}else{
		$scope.seg = '';document.getElementById('pagarcarrito').innerHTML= '<i style="font-size:35px; padding: 15px 0px 15px 0px; height:35px; color:#0066CC" class="right ion-android-restaurant"></i>';};

	var t = setTimeout(function(){reloj()},500);
	

$scope.seg = s;
}
 reloj();

function actualizarHora(i) {

    if (i<10) {i = "0" + i};  // Añadir el cero en números menores de 10

    return i;

}
	
	var urlfirebase = "https://tazon.firebaseio.com/";
		var refh = new Firebase(urlfirebase);
		refh.onAuth(function(authData) {
		if (authData) {
		$scope.ID = authData.uid;
		} else {
        console.log("Client unauthenticated.");
		}
		});
	var ref = new Firebase("https://tazon.firebaseio.com/prefactura/"+$scope.ID+"/");
	var fireb =$firebaseArray(ref);
	
	ref.on("value", function(allMessagesSnapshot) {
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
		aa=messageSnapshot.child("state").val();
		//bb=messageSnapshot.child("precio").val();
		console.log(aa.add);
		$scope.total=aa.total;
		$scope.state=aa.add;
		//if(aa.add != 'pre'){$state.go('food'); };
		ref.on("value", function(allMessagesSnapshot) {
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
		aa=messageSnapshot.child("state").val();
		//bb=messageSnapshot.child("precio").val();
		console.log(aa.add);
		$scope.total=aa.total;
		$scope.state=aa.add;
		console.log(aa.add);
		console.log(aa.add);
		
		//if(aa.add != 'pre'){$state.go('food'); };
		if($rootScope.contP != ''){
		if(aa.id == $rootScope.contP){
			$scope.pre_id=messageSnapshot.key();
			console.log(messageSnapshot.key());
			$scope.precio=aa.total;
			
		};	}else{if(aa.add == 'pre'){
			$scope.pre_id=messageSnapshot.key();
			console.log(messageSnapshot.key());
			$scope.precio=aa.total;
		};}
		
	  }); 
	});
	  }); 
	});
	
	 var usuario = new Firebase("https://tazon.firebaseio.com/user/"+$scope.ID+"/");
	usuario.on("value", function(allMessagesSnapshot) {
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
		$scope.nombre=messageSnapshot.child("nombre").val(); 
		$scope.apellido=messageSnapshot.child("apellido").val(); 
		$scope.numero=messageSnapshot.child("numero").val(); 
		
		});
	});
	
	 $scope.pagar = function(){
		var refa = new Firebase("https://tazon.firebaseio.com/prefactura/"+$scope.ID+"/"+$scope.pre_id+"/state/");
		var refac = new Firebase("https://tazon.firebaseio.com/pedido/"+$scope.ID+"/state/");
		var ing = {  
			"add": "pag",
			"nombre":$scope.nombre,
			"apellido":$scope.apellido,
			"numero":$scope.numero
		};
		$state.go('food');
		refa.update(
			ing
		);
		refac.update(
		{"pedido":"true",
		"delivery":"true"
		}
		);
		 
	 }
	
});
app.controller('LugarCtrl', function($rootScope, $http,$compile, NoteStore,$scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray){
	
	//----
	var urlfirebase = "https://tazon.firebaseio.com/";
		var refh = new Firebase(urlfirebase);
		refh.onAuth(function(authData) {
		if (authData) {
		$scope.ID = authData.uid;
		} else {
        console.log("Client unauthenticated.");
		}
		});
	//---------------------------------------
	//---------------------------------------
	//---------------------------------------
	
	var miUbicacion = {}
	initMap = function(){
		var mapDiv = document.getElementById('map');
		
		var mapOptions={center: new google.maps.LatLng(40.4170339,-3.683689),
        zoom: 14, 
        mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDoubleClickZoom: true}
		$scope.map = new google.maps.Map(mapDiv, mapOptions);
		
		//$scope.locateMe();
		
	}
	
	
	
	$scope.locateMe = function(){
		navigator.geolocation.getCurrentPosition(function(pos){
			miUbicacion.lat = pos.coords.latitude;
			miUbicacion.lng = pos.coords.longitude;
			
			console.log(pos.coords.latitude+","+pos.coords.longitude);
			
			//VERIFICAR DIRECCION
			/*
			if((pos.coords.latitude > '25.669723')&&(pos.coords.latitude < '25.760760')){
				if((pos.coords.longitude < '-80.326476')&&(pos.coords.longitude > '-80.465918 ')){
					
				}else{console.log('No esta disponible para tu Zona');}
			}else{console.log('No esta disponible para tu Zona');}
			*/
			//*********
			$scope.lat=pos.coords.latitude;
			$scope.lng=pos.coords.longitude;
			//*********		
			console.log($scope.lat+","+$scope.lng);
			$scope.map.setCenter(miUbicacion);
			//addMarker(miUbicacion);
			var marker = new google.maps.Marker({
				map:$scope.map,
				position: miUbicacion,
				draggable: false,
				animation: google.maps.Animation.DROP
			});
			google.maps.event.trigger($scope.map, 'resize');
			//-----
			var geocoder = new google.maps.Geocoder;
			var latLng = new google.maps.LatLng(miUbicacion.lat, miUbicacion.lng);
			geocoder.geocode({'location': latLng}, function(results, status){
			if(status === google.maps.GeocoderStatus.OK){
				var placeAddress = results[1].formatted_address;
				$scope.dir=placeAddress;
				console.log($scope.dir);
			}
		})
		},function(error){
			
		})
	}
	
	$scope.locateMe();
	if(document.readyState === "complete"){
		initMap();
	}else{
		google.maps.event.addDomListener(window,'load', initMap);
	}
	
	
	var ref = new Firebase("https://tazon.firebaseio.com/prefactura/"+$scope.ID+"/");
	var fireb =$firebaseArray(ref);
	
	ref.on("value", function(allMessagesSnapshot) {
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
		aa=messageSnapshot.child("state").val();
		console.log($rootScope.contP); 
		if($rootScope.contP != ''){
		if(aa.id == $rootScope.contP){
			console.log(messageSnapshot.key()); 
			$scope.id=messageSnapshot.key();
			$scope.precio=aa.total;
			
		};	}else{
		if(aa.add == 'pre'){
			console.log(messageSnapshot.key()); 
			$scope.id=messageSnapshot.key();
			$scope.precio=aa.total;
			
		};
		};
	  }); 
	}); 
	
	
	
	 $scope.pagarf = function(){
		var ref = new Firebase("https://tazon.firebaseio.com/prefactura/"+$scope.ID+"/"+$scope.id+"/state/");
		var fireb =$firebaseArray(ref);
		
		/*if((miUbicacion.lat > '25.669723')&&(miUbicacion.lat < '25.760760')){
				if((miUbicacion.lng < '-80.326476')&&(miUbicacion.lng > '-80.465918 ')){
					
				}else{console.log('No esta disponible para tu Zona');}
			}else{console.log('No esta disponible para tu Zona');}*/
			var ing = {  
							"lat": $scope.lat,
							"lng": $scope.lng,
							"direccion": $scope.dir
						};
						
						ref.update(
							ing
						);
					$state.go('pagar');
		
	 }
	  
	
});

app.controller('PedidosCtrl', function($http,$compile, NoteStore,$scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray){
	
	var urlfirebase = "https://tazon.firebaseio.com/";
		var refh = new Firebase(urlfirebase);
		refh.onAuth(function(authData) {
		if (authData) {
		$scope.ID = authData.uid;
		} else {
        console.log("Client unauthenticated.");
		}
		});
		
	var fb2 = new Firebase("https://tazon.firebaseio.com/prefactura/");
	fb2.on("value", function(allMessagesSnapshot) {
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
		$scope.comidas = $firebaseArray(fb2);
	  }); 
	}); 
		
	
		
	$scope.preparado = function(info,info2){
	$scope.id_user=info.$id;
	$scope.id_comida=info2;
	
	var fb4 = new Firebase("https://tazon.firebaseio.com/prefactura/"+$scope.id_user+"/");
	fb4.on("value", function(allMessagesSnapshot) {var cont=0;
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
		$scope.comidas = $firebaseArray(fb2);
		bb=messageSnapshot.child("state").val();
		if(bb.add == 'pag'){
			console.log(bb.id);
			console.log(messageSnapshot.key());
			console.log("AAA");
			cont=cont+1;
			console.log(cont);
			
		
		};
		
		aa=messageSnapshot.child("state").val();
		if($scope.id_comida ==aa.id){
			console.log(aa.id);
			console.log(messageSnapshot.key());
			$scope.listo=messageSnapshot.key();
			};
		
	  });
		if(cont==0){
			var asd={
				"pedido":false	,
				"delivery":true
			}
			var fb5 = new Firebase("https://tazon.firebaseio.com/pedido/"+$scope.id_user+"/state/");
			fb5.update(
			asd
			);
			
		}
	  
	});
	var jss={
		"add":"pagado"	
	}
	var fb5 = new Firebase("https://tazon.firebaseio.com/prefactura/"+$scope.id_user+"/"+$scope.listo+"/state/");
	fb5.update(
	jss
	);
	
	}
		
});
app.controller('RegistroCtrl', function($http,$compile, NoteStore,$scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray){
	var urlfirebase = "https://tazon.firebaseio.com/";
		var refh = new Firebase(urlfirebase);
		refh.onAuth(function(authData) {
		if (authData) {
		$scope.ID = authData.uid;
		$scope.email= authData.password.email;
		} else {
        console.log("Client unauthenticated.");
		}
		});
	$scope.aux2=true;
	var ref2 = new Firebase("https://tazon.firebaseio.com/user/"+$scope.ID+"/");
	var ref3 = new Firebase("https://tazon.firebaseio.com/user/");
	$scope.error=false;
	var ref = new Firebase("https://tazon.firebaseio.com/user/"+$scope.ID+"/datos");
	var fireb =$firebaseArray(ref2);
	$scope.inf=fireb; 
	
	ref3.on("value", function(allMessagesSnapshot) {
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
		
		if(messageSnapshot.key() == $scope.ID){$scope.aux2=false;
		console.log(messageSnapshot.key()); };
		
		
	  }); 
	}); 
	
	
	
	
	$scope.aux = $scope.aux2;
	$scope.infor = function(inf){
		var ing = {  
			"nombre": inf.nombre,
			"apellido": inf.apellido,
			"numero": inf.numero
		};
		
		
		
		
		//console.log($scope.info.numero);
		//ref.update($scope.todo);  
		if((inf.numero =='')||(inf.nombre =='')||(inf.apellido =='')){
			
			
			$scope.error=true;console.log(inf);
			

		}else{
			$state.go('food');
			ref.update(ing);};
		 
		 
	}
	
});
app.controller('EsperaCtrl', function($http,$compile, NoteStore,$scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray){
	var urlfirebase = "https://tazon.firebaseio.com/";
		var refh = new Firebase(urlfirebase);
		refh.onAuth(function(authData) {
		if (authData) {
		$scope.ID = authData.uid;
		} else {
        console.log("Client unauthenticated.");
		}
		});
	
	var ref = new Firebase("https://tazon.firebaseio.com/prefactura/"+$scope.ID+"/");
	var fireb =$firebaseArray(ref);
	

		
	
		
		
		ref.on("value", function(allMessagesSnapshot) {
		
$scope.comidas = fireb;
		 
	});

	
	
	$scope.direccion = function(lat,lng){
		var aux = 'TEST';
		return aux;
	}
	
});

app.controller('EntregasCtrl', function($rootScope,$http,$compile, NoteStore,$scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray){
	var urlfirebase = "https://tazon.firebaseio.com/";
		var refh = new Firebase(urlfirebase);
		refh.onAuth(function(authData) {
		if (authData) {
		$scope.ID = authData.uid;
		} else {
        console.log("Client unauthenticated.");
		}
		});
	var fb2 = new Firebase("https://tazon.firebaseio.com/prefactura/");
	fb2.on("value", function(allMessagesSnapshot) {
		$scope.comidas = $firebaseArray(fb2);
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
		
	  }); 
	}); 
	
	$scope.entregado = function(info,info2,info3){
	$scope.id_user=info.$id;
	$scope.id_comida=info2;
	console.log(info[0]);
	console.log(info3);
	console.log(info2);
	var fb4 = new Firebase("https://tazon.firebaseio.com/prefactura/"+$scope.id_user+"/");
	fb4.on("value", function(allMessagesSnapshot) {
		var cont=0;
	$scope.comidas = $firebaseArray(fb2);
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
		
		bb=messageSnapshot.child("state").val();
		if(bb.add == 'pagado'){
			console.log(bb.id);
			console.log(messageSnapshot.key());
			console.log("AAA");
			cont=cont+1;
			console.log(cont);
			
		
		};
		
		aa=messageSnapshot.child("state").val();
		if($scope.id_comida ==aa.id){
			console.log(aa.id);
			console.log(messageSnapshot.key());
			$scope.listo=messageSnapshot.key();
			};
		
	  });
		if(cont==0){
			var asd={
				"pedido":false	,
				"delivery":false	
			}
			var fb5 = new Firebase("https://tazon.firebaseio.com/pedido/"+$scope.id_user+"/state/");
			fb5.update(
			asd
			);
			
		}
	  
	});
	//var jss={"add":"entregado"	}
	//var fb5 = new Firebase("https://tazon.firebaseio.com/prefactura/"+$scope.id_user+"/"+$scope.listo+"/state/");
	//fb5.update(jss);
	
	var fb6 = new Firebase("https://tazon.firebaseio.com/prefactura/"+$scope.id_user+"/"+$scope.listo+"/");
	
	var fb7 = new Firebase("https://tazon.firebaseio.com/historial/"+$scope.id_user+"/");

	var fireb7 =$firebaseArray(fb7);

	fireb7.$add(info3);  
	
	fb6.remove(); 
	}
	/*
	var directionsService = new google.maps.DirectionsService();
	var directionsDisplay = new google.maps.DirectionsRenderer();
	var myOptions = {
     zoom:7,
     mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	var map = new google.maps.Map(document.getElementById("map4"), myOptions);
	directionsDisplay.setMap(map);
	
	var fb6 = new Firebase("https://tazon.firebaseio.com/prefactura/");
	fb6.on("value", function(allMessagesSnapshot) {
		
	$scope.tiempo = function(lat,lng){
	var request = {
       origin: 'avenida piar guacara', 
       destination: new google.maps.LatLng(lat,lng),
       travelMode: google.maps.DirectionsTravelMode.DRIVING
	};
	directionsService.route(request, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {

			$scope.distancia= response.routes[0].legs[0].distance.value/1000;
			$scope.duracion=response.routes[0].legs[0].duration.value/60;
			
console.log($scope.distancia+'-----------');
         directionsDisplay.setDirections(response);
		
      }
	});
	
		console.log(lat+''+lng);
	}
	});*/
	
	
	$scope.ir=function(aux,aux2){
		$rootScope.direc=aux;
		$rootScope.iid=aux2;
		$state.go('entregar');
	}
});

app.controller('EntregarTCtrl', function($rootScope,$http,$compile, NoteStore,$scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray){
	var urlfirebase = "https://tazon.firebaseio.com/";
		var refh = new Firebase(urlfirebase);
		refh.onAuth(function(authData) {
		if (authData) {
		$scope.ID = authData.uid;
		} else {
        console.log("Client unauthenticated.");
		}
		});
	
	 //var directions = new google.maps.DirectionsService();
      //var renderer = new google.maps.DirectionsRenderer();
	
	var directionsService = new google.maps.DirectionsService();
	var directionsDisplay = new google.maps.DirectionsRenderer();
	var miUbicacion = {}
	initMap = function(){
		var mapDiv = document.getElementById('map3');
		 
		var mapOptions={
			center: new google.maps.LatLng(40.4170339,-3.683689),
			zoom: 14, 
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDoubleClickZoom: false
			}
		$scope.map = new google.maps.Map(mapDiv, mapOptions);
		
		$scope.locateMe();
		//$scope.locateMe();
		
		
	}
	
	
	
	$scope.locateMe = function(){
		navigator.geolocation.getCurrentPosition(function(pos){
			miUbicacion.lat = pos.coords.latitude;
			miUbicacion.lng = pos.coords.longitude;
			
			console.log(pos.coords.latitude+","+pos.coords.longitude);
			
			//VERIFICAR DIRECCION
			/*
			if((pos.coords.latitude > '25.669723')&&(pos.coords.latitude < '25.760760')){
				if((pos.coords.longitude < '-80.326476')&&(pos.coords.longitude > '-80.465918 ')){
					
				}else{console.log('No esta disponible para tu Zona');}
			}else{console.log('No esta disponible para tu Zona');}
			*/
			//*********
			$scope.lat=pos.coords.latitude;
			$scope.lng=pos.coords.longitude;
			//*********		
			console.log($scope.lat+","+$scope.lng);
			$scope.map.setCenter(miUbicacion);
			//addMarker(miUbicacion);
			var marker = new google.maps.Marker({
				map:$scope.map,
				position: miUbicacion,
				draggable: false,
				animation: google.maps.Animation.DROP
			});
			google.maps.event.trigger($scope.map, 'resize');
			traceRoute();
			//-----
			var geocoder = new google.maps.Geocoder;
			var latLng = new google.maps.LatLng(miUbicacion.lat, miUbicacion.lng);
			geocoder.geocode({'location': latLng}, function(results, status){
			if(status === google.maps.GeocoderStatus.OK){
				var placeAddress = results[1].formatted_address;
				$scope.dir=placeAddress;
				console.log($scope.dir);
			}
		})
		},function(error){
			
		})
	}

	
	
	
	//*************************
	/*waypts.push({
	location:as2});
	waypts.push({
	location:as3, stopover:true});*/
	//------------------------------------------
	
	

	var fb4 = new Firebase("https://tazon.firebaseio.com/prefactura/");
	fb4.on("value", function(allMessagesSnapshot) {
		$scope.waypts=[];
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
		messageSnapshot.forEach(function(messageSnapshot2) { 
		bb=messageSnapshot2.child("state").val();
		console.log(bb.add); 
		if(bb.add == 'pagado'){ 
			console.log(bb.add);
			console.log(bb.lat+','+bb.lng);
			$scope.waypts.push({location:new google.maps.LatLng(bb.lat,bb.lng)});
			//$scope.waypts=[{location:new google.maps.LatLng(bb.lat,bb.lng)}];
		}
		console.log($scope.waypts[0]);
		
	  });
		
	  });

	  
	traceRoute = function(){
		var directionsDisplay = new google.maps.DirectionsRenderer();
		directionsDisplay.setMap($scope.map);
		var ub={lat: miUbicacion.lat, lng:miUbicacion.lng+1}
		console.log($scope.waypts[0]+'***********************************');
		var request = {
			origin: new google.maps.LatLng(miUbicacion), 
			destination:  new google.maps.LatLng(miUbicacion),
			waypoints: $scope.waypts,
			travelMode : google.maps.TravelMode.DRIVING,
			provideRouteAlternatives: true
		}
		
		var directionsService = new google.maps.DirectionsService();
		
		directionsService.route(request, function(response, status){
			if(status == google.maps.DirectionsStatus.OK){
				directionsDisplay.setDirections(response);
			}
		})
		//-----
	
	var panel = document.getElementById('panel2');
        panel.innerHTML = '';
        directionsService.route(request, function(response, status) {
          if (status == google.maps.DirectionsStatus.OK) {
		  
		  
		  
            directionsDisplay.setDirections(response);
            directionsDisplay.setMap($scope.map);
            directionsDisplay.setPanel(panel);
          
		 
		  } else {
            directionsDisplay.setMap(null);
            directionsDisplay.setPanel(null);
          }
        });
	
	//-----
	}
	
	if(document.readyState === "complete"){
		initMap();
	}else{
		google.maps.event.addDomListener(window,'load', initMap);
	}
	});
	
	
	
	//------------------------------------------
	
});

app.controller('EntregarCtrl', function($rootScope, $stateParams,$http,$compile, NoteStore,$scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray){
	var urlfirebase = "https://tazon.firebaseio.com/";
		var refh = new Firebase(urlfirebase);
		refh.onAuth(function(authData) {
		if (authData) {
		$scope.ID = authData.uid;
		} else {
        console.log("Client unauthenticated.");
		}
		});

	var directionsService = new google.maps.DirectionsService();
	var directionsDisplay = new google.maps.DirectionsRenderer();
	var miUbicacion = {}
	initMap2 = function(){
		var mapDiv = document.getElementById('map5');
		 
		var mapOptions={
			center: new google.maps.LatLng(40.4170339,-3.683689),
			zoom: 14, 
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDoubleClickZoom: false
			}
		$scope.map = new google.maps.Map(mapDiv, mapOptions);
		
		$scope.locateMe();

	}
	
	
	
	$scope.locateMe = function(){
		navigator.geolocation.getCurrentPosition(function(pos){
			miUbicacion.lat = pos.coords.latitude;
			miUbicacion.lng = pos.coords.longitude;
			
			console.log(pos.coords.latitude+","+pos.coords.longitude);
			
			//VERIFICAR DIRECCION
			/*
			if((pos.coords.latitude > '25.669723')&&(pos.coords.latitude < '25.760760')){
				if((pos.coords.longitude < '-80.326476')&&(pos.coords.longitude > '-80.465918 ')){
					
				}else{console.log('No esta disponible para tu Zona');}
			}else{console.log('No esta disponible para tu Zona');}
			*/
			//*********
			$scope.lat=pos.coords.latitude;
			$scope.lng=pos.coords.longitude;
			//*********		
			console.log($scope.lat+","+$scope.lng);
			$scope.map.setCenter(miUbicacion);

			var marker = new google.maps.Marker({
				map:$scope.map,
				position: miUbicacion,
				draggable: false,
				animation: google.maps.Animation.DROP
			});
			google.maps.event.trigger($scope.map, 'resize');
			traceRoute();
			//-----
			var geocoder = new google.maps.Geocoder;
			var latLng = new google.maps.LatLng(miUbicacion.lat, miUbicacion.lng);
			geocoder.geocode({'location': latLng}, function(results, status){
			if(status === google.maps.GeocoderStatus.OK){
				var placeAddress = results[1].formatted_address;
				$scope.dir=placeAddress;
				console.log($scope.dir);
			}
		})
		},function(error){
			
		})
	}

	$scope.id_dir=$rootScope.direc;

	var fb4 = new Firebase("https://tazon.firebaseio.com/prefactura/"+$rootScope.iid.$id+"/");
	fb4.on("value", function(allMessagesSnapshot) {
		console.log(allMessagesSnapshot);
		allMessagesSnapshot.forEach(function(messageSnapshot) { 
		//console.log(messageSnapshot.key());
		var aa=messageSnapshot.child("state").val()
		//console.log(aa.id);
		if(aa.id == $scope.id_dir){
			$scope.lat2=aa.lat;
			$scope.lng2=aa.lng;
			console.log('AAAAA');
		}
		
		}); 
		
		
		//console.log($rootScope.direc);
		//console.log($rootScope.iid.$id);
	 
	traceRoute = function(){
		var directionsDisplay = new google.maps.DirectionsRenderer();
		directionsDisplay.setMap($scope.map);
		var ub={lat: miUbicacion.lat, lng:miUbicacion.lng}
		var request = {
			origin: new google.maps.LatLng(miUbicacion), 
			destination: new google.maps.LatLng($scope.lat2,$scope.lng2) ,
			travelMode : google.maps.TravelMode.DRIVING,
			provideRouteAlternatives: true
		}
		
		var directionsService = new google.maps.DirectionsService();
		
		directionsService.route(request, function(response, status){
			if(status == google.maps.DirectionsStatus.OK){
				directionsDisplay.setDirections(response);
			}
		})
		//-----
	
	var panel = document.getElementById('panel3');
        panel.innerHTML = '';
        directionsService.route(request, function(response, status) {
          if (status == google.maps.DirectionsStatus.OK) {
		  
		  
		  
            directionsDisplay.setDirections(response);
            directionsDisplay.setMap($scope.map);
            directionsDisplay.setPanel(panel);
          
		 
		  } else {
            directionsDisplay.setMap(null);
            directionsDisplay.setPanel(null);
          }
        });
	
	//-----
	}
	
	if(document.readyState === "complete"){
		initMap2();
	}else{
		google.maps.event.addDomListener(window,'load', initMap2);
	} 
	});
	
	
	
	//------------------------------------------
	
});

app.controller('HistoPCtrl', function($rootScope,$http,$compile, NoteStore,$scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray){
	var urlfirebase = "https://tazon.firebaseio.com/";
		var refh = new Firebase(urlfirebase);
		refh.onAuth(function(authData) {
		if (authData) {
		$scope.ID = authData.uid;
		} else {
        console.log("Client unauthenticated.");
		}
		});
	var fb2 = new Firebase("https://tazon.firebaseio.com/historial/");
	fb2.on("value", function(allMessagesSnapshot) {
		$scope.comidas = $firebaseArray(fb2);

	}); 

	$scope.ir=function(aux,aux2){
		$rootScope.direc=aux;
		$rootScope.iid=aux2;
		$state.go('entregar');
	}
});

app.controller('HistoCtrl', function($rootScope,$http,$compile, NoteStore,$scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray){
	var urlfirebase = "https://tazon.firebaseio.com/";
		var refh = new Firebase(urlfirebase);
		refh.onAuth(function(authData) {
		if (authData) {
		$scope.ID = authData.uid;
		} else {
        console.log("Client unauthenticated.");
		}
		});
	var fb2 = new Firebase("https://tazon.firebaseio.com/historial/"+$scope.ID+"/");
	fb2.on("value", function(allMessagesSnapshot) {
		$scope.comidas = $firebaseArray(fb2);

	}); 

	$scope.ir=function(aux,aux2){
		$rootScope.direc=aux;
		$rootScope.iid=aux2;
		$state.go('entregar');
	}
});

app.controller('PIncompletoCtrl', function($rootScope,$http,$compile, NoteStore,$scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray){
	var urlfirebase = "https://tazon.firebaseio.com/";
		var refh = new Firebase(urlfirebase);
		refh.onAuth(function(authData) {
		if (authData) {
		$scope.ID = authData.uid;
		} else {
        console.log("Client unauthenticated.");
		}
		});
	
	$scope.contPe=function(aux){
		$rootScope.contP=aux;console.log(aux);
		$state.go("lugar");
	}
	$scope.contPe2=function(aux){
		$rootScope.contP=aux;console.log(aux);
		$state.go("pagar");
	}
		
	var fb2 = new Firebase("https://tazon.firebaseio.com/prefactura/");
	fb2.on("value", function(allMessagesSnapshot) {
		$scope.comidas = $firebaseArray(fb2);
	}); 

	
	
});

app.controller('InfoCtrl', function($http,$compile, NoteStore,$scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray){
		var urlfirebase = "https://tazon.firebaseio.com/";
		var refh = new Firebase(urlfirebase);
		refh.onAuth(function(authData) {
		if (authData) {
		$scope.ID = authData.uid;
		$scope.email=authData.password.email;
		} else {
        console.log("Client unauthenticated.");
		}
		});
		
	var fb = new Firebase("https://tazon.firebaseio.com/user/"+$scope.ID+"/");
	fb.on("value", function(allMessagesSnapshot) {
		$scope.info = $firebaseArray(fb);
	}); 

	
});

app.controller('EditCtrl', function($http,$compile, NoteStore,$scope, $state, $firebaseObject, $ionicPopup, $ionicModal, $firebaseArray){
		var urlfirebase = "https://tazon.firebaseio.com/";
		var refh = new Firebase(urlfirebase);
		refh.onAuth(function(authData) {
		if (authData) {
		$scope.ID = authData.uid;
		} else {
        console.log("Client unauthenticated.");
		}
		});
	
	
});




}());
